<!ENTITY gst-branch-stable "1.20">
<!ENTITY gst-version-stable "1.20.5">
<!ENTITY gst-version-devel "1.21.3">

<!ENTITY orc-version-stable "0.4.33">
<!ENTITY orc-version-devel "git main">

<!ENTITY gst-bug-report "http://bugzilla.gnome.org/enter_bug.cgi?product=GStreamer">
<!ENTITY gst-repo-http "https://gitlab.freedesktop.org/gstreamer/">
<!ENTITY realsite "https://gstreamer.freedesktop.org">
<!ENTITY nbsp "&#x00A0;">
